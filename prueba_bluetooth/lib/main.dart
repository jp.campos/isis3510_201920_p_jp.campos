import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:prueba_bluetooth/RequestFireBase.dart';
import 'dart:async';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:path/path.dart' show join;
import 'package:path_provider/path_provider.dart';

void main() => runApp(MyApp());



class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'BLueTooth',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.indigo,


      ),
      home: MyHomePage(title: 'BonoApp'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);


  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  String _estaPrendido = "Prenda el bluetooth";
  var cameras;
  var firstCamera;

  final FlutterBlue bluetooth = FlutterBlue.instance;

  @override
  void initState() {
    getCameras();
    bluetoothConnectionState();
    super.initState();

  }


  void getCameras() async
  {
    cameras =  await availableCameras();
    firstCamera = cameras.first;
  }

  Future<void> bluetoothConnectionState() async {
    while(true) {

      bool conectado = await bluetooth.isOn;

        setState(() {

        if (conectado) {
          _estaPrendido = "Gracias por prender el bluetooth";
        }else {
          _estaPrendido = "Prenda el bluetooth porfavor";
        }


        });
    }
  }

  void _prenderBluetooth() {


  }




 tomarFoto() async
{


  // Get a specific camera from the list of available cameras.
  final firstCamera = cameras.first;

  return cameras.first;

}



  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),

      ),
      body: Center(

        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(padding: EdgeInsets.symmetric(vertical: 30, horizontal: 30),
            child:
            Text(
              '$_estaPrendido',
              style: Theme.of(context).textTheme.display1,

            ),
        ),
          SizedBox(width: 20, height: 20,), // separacion
            Padding(
              padding: EdgeInsets.symmetric(vertical: 20),
                child: Boton(
                  onPressed: () => Navigator.push(context, MaterialPageRoute(builder:  (context) => PaginaListaEstudiantes()) ) ,
                  icono: Icon(Icons.contact_mail, color: Colors.white70,),texto: "Revisar Asistencia", ancho: 310,tamTexto: 28, ),
            )
            ,

            Boton(onPressed: () => Navigator.push(context, MaterialPageRoute(builder:  (context) => TakePictureScreen(camera: firstCamera,)) ) ,
              icono: Icon(Icons.camera_alt, color: Colors.white70,),texto: "Tomar Foto",ancho: 310, tamTexto: 28,)
          ],

        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _prenderBluetooth,
        tooltip: 'Activar bluetooth',
        child: Icon(Icons.bluetooth),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

class PaginaListaEstudiantes extends StatefulWidget
{
  @override
  _EstadoLista createState() => _EstadoLista();

}

class _EstadoLista extends State<PaginaListaEstudiantes>
{

  final HacerRequest r = HacerRequest();
  List<String> listaEstudiantes= [];
  List<String> listaAusentes =[];
  String fechaSeleccionada;
  Color colorSeleccionado;
  int indiceSel;
  bool fechaSel;
  DateTime selectedDate = DateTime.now();
  @override
  void initState ()
  {
    fechaSel = false;
    super.initState();
    colorSeleccionado = Colors.transparent;
    esperar();
  }


  void esperar () async
  {
    listaEstudiantes = await r.realizarSolicitud("https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/studentsList");
    setState(() {

    });
  }

  void esperarListaAsistentes () async
  {
    String query = "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/"
        "${selectedDate.day}-${selectedDate.month-1}-${selectedDate.year}/students";

    print(query);

    List<String> listaAsistentes = await r.realizarSolicitud(query);
    print(listaAsistentes);
    setListaAusentes(listaEstudiantes, listaAsistentes);
    setState(() {

    });
  }

  void llamarMas(BuildContext context) async
  {
    List<String> temps = await r.siguientePagina("https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/studentsList");
  //print(temps);
    setState(() {
      if(temps.isNotEmpty)
        {
          listaEstudiantes.addAll(temps);
        }else {
         Scaffold.of(context).showSnackBar(new SnackBar(
          content: new Text("No hay más por cargar"),
        ));
      }

    });

  }

  void setListaAusentes (List<String> listaE, List<String> listaAsistentes){
    listaE.forEach((estudiante){
      if(!listaAsistentes.contains(estudiante))
        {
          listaAusentes.add(estudiante);
        }

    });

  }

  void guardar() async
  {
    File file = await writeFile();


  }

  Future<File> writeFile() async {
    final file = await _localFile;

    listaAusentes.forEach((estudiante)
    {
      file.writeAsString(estudiante);

    });

    return file;
  }

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/lista.txt');
  }


  Future<Null> elegirFecha(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        fechaSel = true;
        esperarListaAsistentes();
      });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Asistencia"),
        ),
        body: Builder(builder: (context) {

          if(listaAusentes.isEmpty && fechaSel)
            {
              return Center(
                  child: CircularProgressIndicator()
              );
            }

         return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget> [

          Row(
           children: <Widget> [

             Padding(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 5),
              child:
              Boton(texto: "Guardar",icono: Icon(Icons.save, color: Colors.white70,),
              ancho: 150,
                tamTexto: 15,
                onPressed: guardar,
              ),
            ),
             Padding(
               padding: EdgeInsets.symmetric(horizontal: 15),
                 child: Boton(

                   texto: "Elegir Fecha",
                   icono: Icon(Icons.calendar_today, color: Colors.white70,),
                   onPressed: () => elegirFecha(context),
                   ancho: 150,


                 ),)

           ]),

            SizedBox(
                height: 500,
            child:Padding (
                padding: EdgeInsets.symmetric(horizontal: 10, ),
                child: ListView.builder(itemCount: listaAusentes.length,itemBuilder: (BuildContext context, int index) => BuildCoso(context, index)))
    ),
            FlatButton(
              splashColor: Colors.grey,
              child: Text("Cargar Más...", style: TextStyle(fontSize: 20,color: Colors.indigoAccent),),
              onPressed: () => llamarMas(context),
              
            )
        ]);})
    );
  }




Widget  BuildCoso(BuildContext contexto, int index){
  return new Container(

    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20,),
    decoration: BoxDecoration(border: Border(bottom: BorderSide(color: Colors.black26)),
        color: indiceSel != null && indiceSel == index ? Colors.black12: Colors.transparent),
    child: ListTile(

      title: Text(listaEstudiantes[index], style: TextStyle(fontSize: 27), ),
      onTap: (){
          setState(() {

            indiceSel = index;
          });
        },
      ),


  );

}

}//PaginaListaEstudiantes




// A screen that allows users to take a picture using a given camera.
class TakePictureScreen extends StatefulWidget {
  final CameraDescription camera;

  const TakePictureScreen({
    Key key,
    @required this.camera,
  }) : super(key: key);

  @override
  TakePictureScreenState createState() => TakePictureScreenState();
}

class TakePictureScreenState extends State<TakePictureScreen> {
  CameraController _controller;
  Future<void> _initializeControllerFuture;

  @override
  void initState() {
    super.initState();
    // To display the current output from the Camera,
    // create a CameraController.
    _controller = CameraController(
      // Get a specific camera from the list of available cameras.
      widget.camera,
      // Define the resolution to use.
      ResolutionPreset.medium,
    );

    // Next, initialize the controller. This returns a Future.
    _initializeControllerFuture = _controller.initialize();
  }

  @override
  void dispose() {
    // Dispose of the controller when the widget is disposed.
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Take a picture')),
      // Wait until the controller is initialized before displaying the
      // camera preview. Use a FutureBuilder to display a loading spinner
      // until the controller has finished initializing.
      body: FutureBuilder<void>(
        future: _initializeControllerFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            // If the Future is complete, display the preview.
            return CameraPreview(_controller);
          } else {
            // Otherwise, display a loading indicator.
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.camera_alt),
        // Provide an onPressed callback.
        onPressed: () async {
          // Take the Picture in a try / catch block. If anything goes wrong,
          // catch the error.
          try {
            // Ensure that the camera is initialized.
            await _initializeControllerFuture;

            // Construct the path where the image should be saved using the
            // pattern package.
            final path = join(
              // Store the picture in the temp directory.
              // Find the temp directory using the `path_provider` plugin.
              (await getTemporaryDirectory()).path,
              '${DateTime.now()}.png',
            );

            // Attempt to take a picture and log where it's been saved.
            await _controller.takePicture(path);

            // If the picture was taken, display it on a new screen.
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => DisplayPictureScreen(imagePath: path),
              ),
            );
          } catch (e) {
            // If an error occurs, log the error to the console.
            print(e);
          }
        },
      ),
    );
  }
}

// A widget that displays the picture taken by the user.
class DisplayPictureScreen extends StatelessWidget {
  final String imagePath;

  const DisplayPictureScreen({Key key, this.imagePath}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Display the Picture')),
      // The image is stored as a file on the device. Use the `Image.file`
      // constructor with the given path to display the image.
      body: Image.file(File(imagePath)),
    );
  }
}

// ------------------------------------------------WIDGETS ------------------------------------------------

class Boton extends StatelessWidget{

  String texto;
  Icon icono;
  final GestureTapCallback onPressed;
  double ancho;
  double tamTexto = 28;
  Boton({
    this.texto,
    this.icono,
    @required
    this.onPressed,
    this.ancho,
    this.tamTexto
});


  @override
  Widget build(BuildContext context) {

    return Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget> [
        Container(
          width: ancho,
          child: FlatButton(
            color: Colors.indigoAccent,

            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 8),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget> [
                    icono,
                    Padding(padding: EdgeInsets.symmetric(horizontal: 8),

                      child: Text(texto, style: TextStyle(color: Colors.white, fontSize: tamTexto, fontWeight: FontWeight.w300),
                      ),),

                  ]


              ),),

            onPressed: onPressed,
            shape: StadiumBorder(),

          ))
        ]);
  }

}