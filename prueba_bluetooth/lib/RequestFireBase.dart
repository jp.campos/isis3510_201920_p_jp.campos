import 'dart:async';
import 'dart:convert';


import 'package:http/http.dart' as http;



class HacerRequest
{

  String tokenSiguiente;

  Future<List<String>> realizarSolicitud(String url) async
  {

    final response =  await http.get(url);
    List<String> lista = [];
    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON.
      Pagina p = Pagina.fromJson(json.decode(response.body));


      tokenSiguiente = p.nextPageToken;

      while(tokenSiguiente!= null) {

        List<Documento> docs = p.documentos;
        final response2 =  await http.get(url + "?pageToken=$tokenSiguiente");

        List<String> listaTemp = docs.map((i) => i.fields.name.stringValue)
            .toList();

        lista.addAll(listaTemp);
        print("En realizar solizitud $listaTemp");

        p = Pagina.fromJson(json.decode(response2.body));
        tokenSiguiente = p.nextPageToken;

      }

    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }

    return lista;
  }

  Future<List<String>> siguientePagina(String url) async
  {

    final response2 =  await http.get(url + "?pageToken=$tokenSiguiente");
    List<String> lista = [];
    if (response2.statusCode == 200) {
      // If the call to the server was successful, parse the JSON.
      Pagina p = Pagina.fromJson(json.decode(response2.body));

      if(tokenSiguiente== null)
        {

        }

      List<Documento> docs = p.documentos;

      List<String> listaTemp = docs.map((i) => i.fields.name.stringValue).toList();

      lista.addAll(listaTemp);


      tokenSiguiente = p.nextPageToken;


    } else {
      // If that call was not successful, throw an error.
      return List<String>();
    }

    return lista;
  }

}//HacerRequest





class Pagina {
  final List<Documento> documentos;
  final String nextPageToken;

  Pagina({this.documentos, this.nextPageToken});

  factory Pagina.fromJson(Map<String, dynamic> json) {

    var listaDocs = json['documents'] as List;

    List<Documento> lista = listaDocs.map((i) => Documento.fromJson(i)).toList();

    return Pagina(
      documentos: lista,
      nextPageToken: json['nextPageToken'],
    );
  }
}

class Documento{

  final Fields fields;

  Documento({
   this.fields
});


  factory Documento.fromJson(Map<String, dynamic> json)
  {

    return Documento(
      fields: Fields.fromJson(json["fields"])
    );
  }

}



class Fields{

  final Name name;

  Fields({
   this.name
  });

  factory Fields.fromJson(Map<String, dynamic> parsedJson){
    return Fields(
        name: parsedJson != null ? Name.fromJson(parsedJson["name"]) : Name(stringValue: "null")
    );
  }



}

class Name{

  final String stringValue;

  Name({
    this.stringValue
  });

  factory Name.fromJson(Map<String, dynamic> parsedJson){
    return Name(
        stringValue: parsedJson['stringValue']
    );
  }
}